### how can you contribute ?
this repo main purpose is to document my personal experiences of life or apps. period. as of now, i think there are only two ways for contribution ( correct me if i am wrong ) :
- typos in posts or files
- enhancement

### how can i contribute to your project ?

#### my limitations
- [x] no access to desktop or laptop
- [x] no writing skills, for example to write document
- [x] intemperate

#### my strengths
- [x] observant
- [x] does not hesitate to ask if i am unsure

#### my imaginative roles
- [x] creating labels for issues or merge requests
- [x] report issues which i cannot resolve myself
- [x] translations [ you must resolve merge request conflicts ], if you are OK using [deepl](https://www.deepl.com/translator), [bing](https://www.bing.com/translator/), [google](https://translate.google.com/), or anyother online resource

#### contact me
- [x] krishna@grrlz.net
- [x] @leela@mastodon.social