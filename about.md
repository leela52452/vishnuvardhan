---
layout: page
title: About
permalink: /about/
---

**who am i ?**
`a straightforward person`

**what is my purpose or aim ?**
`to share my thoughts and musings`
 
**where am i going ?**
`to my innermost consciousness`

**when will i stop ?**
`there is nothing to share anymore`

**why am i updating ?**
`to share with everybody without any restrictions`

**how frequently will i update ?**
`as situation demands`

comments, suggestions, criticism, etc..: `@leela@mastodon.social`